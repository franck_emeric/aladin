import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AladinComponent } from './aladin/aladin.component';
import { AladinProductsComponent } from './aladin-products/aladin-products.component';
import { AladinFacesComponent } from './aladin-faces/aladin-faces.component';
import { AladinDesignsPacksComponent } from './aladin-designs-packs/aladin-designs-packs.component';
import { AladinDesignsClothesComponent } from './aladin-designs-clothes/aladin-designs-clothes.component';
import { AladinDesignsComponent } from './aladin-designs/aladin-designs.component';
import { AladinShapesComponent } from './aladin-shapes/aladin-shapes.component';
import { AladinClipartsComponent } from './aladin-cliparts/aladin-cliparts.component';
import { AladinTextComponent } from './aladin-text/aladin-text.component';
import { ColorCircleModule } from 'ngx-color/circle'; 
import { FormsModule } from '@angular/forms';
import { AladinProductsPacksComponent } from './aladin-products-packs/aladin-products-packs.component';
import { AladinModelsComponent } from './aladin-models/aladin-models.component';
import { AladinProductsClothesComponent } from './aladin-products-clothes/aladin-products-clothes.component';



@NgModule({
  declarations: [
    AladinComponent,
    AladinProductsComponent,
    AladinFacesComponent,
    AladinDesignsPacksComponent,
    AladinDesignsClothesComponent,
    AladinDesignsComponent,
    AladinShapesComponent,
    AladinClipartsComponent,
    AladinTextComponent,
    AladinProductsPacksComponent,
    AladinModelsComponent,
    AladinProductsClothesComponent,
  ],
  exports:[
    AladinComponent,
    AladinProductsComponent,
    AladinFacesComponent,
    AladinDesignsPacksComponent,
    AladinDesignsClothesComponent,
    AladinDesignsComponent,
    AladinShapesComponent,
    AladinClipartsComponent,
    AladinTextComponent,


  ],
  imports: [
    CommonModule,
    ColorCircleModule,
    FormsModule
  ],
  
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class SharededitorModule { }
